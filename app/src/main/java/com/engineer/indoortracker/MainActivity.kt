package com.engineer.indoortracker

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.wifi.WifiManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val manager: WifiManager = application.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val myReceiver = MainActivity.MyReceiver(manager, ssidTextView, rssiTextView)
        ssidTextView.text = manager.connectionInfo.ssid
    }

    class MyReceiver(val manager: WifiManager, val ssid: TextView, val rssi: TextView) : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            val myAction : String = intent?.action ?: "no_action"
            if(myAction == WifiManager.RSSI_CHANGED_ACTION) {
                rssi.text = intent?.getIntExtra(WifiManager.EXTRA_NEW_RSSI, 0)?.let { WifiManager.calculateSignalLevel(it, 100).toString() }
            }
            else if(myAction == WifiManager.NETWORK_IDS_CHANGED_ACTION) {
                ssid.text = manager.connectionInfo.ssid
            }
        }
    }
}
